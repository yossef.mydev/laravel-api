<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Todo;
use App\User;
use Faker\Generator as Faker;

$factory->define(Todo::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, User::count()),
        'title' => $faker->sentence(),
        'completed' => $faker->boolean(),
    ];
});
